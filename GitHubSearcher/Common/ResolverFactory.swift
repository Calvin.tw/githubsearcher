//
//  ResolverFactory.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

//swiftlint:disable superfluous_disable_command
//swiftlint:disable force_cast
//swiftlint:disable function_body_length
//swiftlint:disable line_length
//swiftlint:disable vertical_whitespace
//swiftlint:disable identifier_name
//swiftlint:disable file_length

import Swinject
import RxSwift
import RxCocoa

enum ResolverFactory {
    static func make() -> Assembler {
        guard NSClassFromString("XCTest") == nil else {
            return Assembler([])
        }

        return Assembler(
            [
                APIWorkerAssembly(),
                SearchPageAssembly(),
                DetailPageAssembly(),
                RepositoriesPageAssembly()
            ]
        )
    }
}
