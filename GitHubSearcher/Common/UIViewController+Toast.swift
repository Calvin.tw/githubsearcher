//
//  UIViewController+Toast.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

extension UIViewController {
    func messageToast(
        message: String,
        color: UIColor = .red
    ) {
        var topSafeArea: CGFloat = 0
        if #available(iOS 11.0, *) {
            topSafeArea = view.safeAreaInsets.top
        }
        let size = CGSize(width: view.frame.width - 20, height: 50)
        let hideRect = CGRect(origin: CGPoint(x: 10 , y: topSafeArea - size.height - 10), size: size)
        let showRect = CGRect(origin: CGPoint(x: 10 , y: topSafeArea + 10), size: size)
        let messageButton = UIButton(frame: hideRect)
        messageButton.setTitle(message, for: .normal)
        messageButton.tintColor = .white
        messageButton.titleLabel?.font = .systemFont(ofSize: 14)
        messageButton.backgroundColor = color
        messageButton.layer.cornerRadius = 5

        view.addSubview(messageButton)

        UIView.animate(withDuration: 0.3) {
            messageButton.frame = showRect
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIView.animate(
                withDuration: 0.3,
                animations: { messageButton.frame = hideRect },
                completion: { _ in messageButton.removeFromSuperview() }
            )
        }
    }
}
