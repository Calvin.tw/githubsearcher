//
//  LoadingCell.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 10.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit

class LoadingCell: UITableViewCell {
    static let identifier = "LoadingCell"

    lazy var indicator = UIActivityIndicatorView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        indicator.stopAnimating()
    }

    func startAnimating() {
        indicator.startAnimating()
    }

    private func setupView() {
        contentView.backgroundColor = .clear
        contentView.addSubview(indicator)
        indicator.style = .gray
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        indicator.widthAnchor.constraint(equalToConstant: 44).isActive = true
        indicator.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
}
