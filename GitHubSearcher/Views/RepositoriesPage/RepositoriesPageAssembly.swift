//
//  RepositoriesPageAssembly.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit
import Swinject

class RepositoriesPageAssembly: Assembly {
    func assemble(container: Container) {
        container.register(RepositoriesPageController.self) { (resolver, inputData: RepositoriesPage.InputData) in
            let viewController = RepositoriesPageController()
            var interactor = resolver.resolve(RepositoriesPageBusinessLogic.self, argument: inputData)
            var presenter = resolver.resolve(RepositoriesPagePresentationLogic.self, argument: inputData)
            presenter?.viewController = viewController
            interactor?.presenter = presenter
            viewController.interactor = interactor
            return viewController
        }

        container.register(RepositoriesPageInteractor.Context.self) { resolver in
            .init(apiWorker: resolver.resolveAPIWorker())
        }

        container.register(RepositoriesPageBusinessLogic.self) { resolver, inputData in
            RepositoriesPageInteractor(
                context: resolver.resolve(RepositoriesPageInteractor.Context.self)!,
                inputData: inputData
            )
        }

        container.register(RepositoriesPagePresentationLogic.self) { _, inputData in
            RepositoriesPagePresenter(inputData: inputData)
        }
    }
}

extension Resolver {
    func resolveRepositoriesPageController(inputData: RepositoriesPage.InputData) -> RepositoriesPageController {
        resolve(RepositoriesPageController.self, argument: inputData)!
    }
}
