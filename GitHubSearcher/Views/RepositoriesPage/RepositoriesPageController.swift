//
//  RepositoriesPageController.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 09.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit

protocol RepositoriesPageDisplayLogic: UIViewController {
    func reload(viewModel: UserRepositoriesModel.ViewModel)
    func next(viewModel: UserRepositoriesModel.ViewModel)
    func error(message: String)
}

final class RepositoriesPageController: UIViewController {
    private let cellIdentifier = "RepositoryCell"

    // Views

    private lazy var topIndicator = UIView(frame: .zero)
    private lazy var repositoriesTableVIew = UITableView(frame: .zero, style: .plain)
    private lazy var repositoriesLabel = UILabel(frame: .zero)

    // Flow

    var interactor: RepositoriesPageBusinessLogic?

    var viewModel: UserRepositoriesModel.ViewModel?

    private var needUpdate = true

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.fetchUserRepositories()
    }

    private func setupViews() {
        view.backgroundColor = .white

        view.addSubview(topIndicator)
        topIndicator.translatesAutoresizingMaskIntoConstraints = false
        topIndicator.backgroundColor = ColorName.bondiBlue.color
        topIndicator.layer.cornerRadius = 5
        topIndicator.heightAnchor.constraint(equalToConstant: 4).isActive = true
        topIndicator.widthAnchor.constraint(equalToConstant: 40).isActive = true
        topIndicator.topAnchor.constraint(equalTo: view.topAnchor, constant: 8).isActive = true
        topIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        view.addSubview(repositoriesLabel)
        repositoriesLabel.translatesAutoresizingMaskIntoConstraints = false
        repositoriesLabel.textColor = ColorName.ebonyClay.color
        repositoriesLabel.font = .systemFont(ofSize: 18, weight: .medium)
        repositoriesLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        repositoriesLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        repositoriesLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 25).isActive = true
        repositoriesLabel.text = "Repositories"

        view.addSubview(repositoriesTableVIew)
        repositoriesTableVIew.translatesAutoresizingMaskIntoConstraints = false
        repositoriesTableVIew.backgroundColor = .white
        repositoriesTableVIew.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        repositoriesTableVIew.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        repositoriesTableVIew.topAnchor.constraint(equalTo: repositoriesLabel.bottomAnchor).isActive = true
        repositoriesTableVIew.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        repositoriesTableVIew.tableFooterView = UIView()
        repositoriesTableVIew.separatorStyle = .none
        repositoriesTableVIew.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        repositoriesTableVIew.register(LoadingCell.self, forCellReuseIdentifier: LoadingCell.identifier)

        repositoriesTableVIew.dataSource = self
        repositoriesTableVIew.delegate = self
    }
}

extension RepositoriesPageController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = viewModel?.items.count, count > 0 else {
            return 0
        }
        return needUpdate ? count + 1 : count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard !isLoadingCell(indexPath: indexPath) else {
            interactor?.fetchNextUserRepositories(page: (viewModel?.currentPage ?? 0) + 1)
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadingCell.identifier, for: indexPath)
            if let cell = cell as? LoadingCell {
                cell.startAnimating()
            }
            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)

        cell.textLabel?.text = viewModel?.items[indexPath.row].repositoryName
        cell.selectedBackgroundView = UIView()
        return cell
    }

    private func isLoadingCell(indexPath: IndexPath) -> Bool {
        indexPath.row >= (viewModel?.items.count ?? 0)
    }
}

extension RepositoriesPageController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        60
    }
}

extension RepositoriesPageController: RepositoriesPageDisplayLogic {
    func reload(viewModel: UserRepositoriesModel.ViewModel) {
        repositoriesLabel.text = "\(viewModel.userName)'s repositories"

        self.viewModel = viewModel
        needUpdate = viewModel.hasMore
        repositoriesTableVIew.reloadData()
    }

    func next(viewModel: UserRepositoriesModel.ViewModel) {
        let newItemCount = viewModel.items.count - (self.viewModel?.items.count ?? 0)
        guard newItemCount > 0 else {
            needUpdate = false
            return
        }
        self.viewModel = viewModel
        let itemsIndexPath = (0...newItemCount - 1).map { IndexPath(row: (viewModel.currentPage - 1) * 10 + $0, section: 0) }
        repositoriesTableVIew.insertRows(at: itemsIndexPath, with: .none)
        needUpdate = viewModel.hasMore
        if !viewModel.hasMore {
            repositoriesTableVIew.reloadData()
        }

        repositoriesTableVIew.setContentOffset(repositoriesTableVIew.contentOffset, animated: false)
    }

    func error(message: String) {
        messageToast(message: message, color: ColorName.roseRed.color)
    }
}
