//
//  ForkCell.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit

class ForkCell: UITableViewCell {
    static let identifier = "ForkCell"

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var avatarIconView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        avatarIconView.layer.cornerRadius = 30
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        avatarIconView.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
    }

    func set(item: ForkItem) {
        userNameLabel.text = item.loginName
        avatarIconView.kf.setImage(with: item.resource)
    }
}
