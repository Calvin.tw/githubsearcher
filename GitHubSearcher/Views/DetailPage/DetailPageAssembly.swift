//
//  DetailPageAssembly.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit
import Swinject

class DetailPageAssembly: Assembly {
    func assemble(container: Container) {
        container.register(DetailPageController.self) { (resolver, inputData: DetailPage.InputData) in
            let viewController = DetailPageController()
            var interactor = resolver.resolve(DetailPageBusinessLogic.self, argument: inputData)
            var presenter = resolver.resolve(DetailPagePresentationLogic.self, argument: inputData)
            var router = resolver.resolve(DetailPageRoutingLogic.self)
            presenter?.viewController = viewController
            interactor?.presenter = presenter
            router?.viewController = viewController
            viewController.interactor = interactor
            viewController.router = router
            return viewController
        }

        container.register(DetailPageInteractor.Context.self) { resolver in
            .init(apiWorker: resolver.resolveAPIWorker())
        }

        container.register(DetailPageBusinessLogic.self) { resolver, inputData in
            DetailPageInteractor(
                context: resolver.resolve(DetailPageInteractor.Context.self)!,
                inputData: inputData
            )
        }

        container.register(DetailPagePresentationLogic.self) { _, inputData in
            DetailPagePresenter(inputData: inputData)
        }

        container.register(DetailPageRoutingLogic.self) { resolver in
            DetailPageRouter(resolver: resolver)
        }
    }
}

extension Resolver {
    func resolveDetailPageController(inputData: DetailPage.InputData) -> DetailPageController {
        resolve(DetailPageController.self, argument: inputData)!
    }
}
