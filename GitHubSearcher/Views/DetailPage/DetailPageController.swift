//
//  DetailPageController.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 09.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol DetailPageDisplayLogic: UIViewController {
    func reload(viewModel: ForksModel.ViewModel)
    func next(viewModel: ForksModel.ViewModel)
    func error(message: String)
}

final class DetailPageController: UIViewController {

    // Views

    private lazy var forksTableVIew = UITableView(frame: .zero, style: .plain)
    private lazy var userNameButton = UIButton(type: .custom)
    private lazy var repositoryNameLabel = UILabel(frame: .zero)
    private lazy var forkLabel = UILabel(frame: .zero)

    // Flow

    var interactor: DetailPageBusinessLogic?

    var router: DetailPageRoutingLogic?

    var viewModel: ForksModel.ViewModel?

    let disposeBag = DisposeBag()

    private var needUpdate = true

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupActions()
        setupNavigation()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.fetchForksList()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.topItem?.title = "Details"
    }

    private func setupViews() {
        view.backgroundColor = .white

        view.addSubview(userNameButton)

        if #available(iOS 11, *) {
            let guide = view.safeAreaLayoutGuide
            userNameButton.topAnchor.constraint(equalTo: guide.topAnchor, constant: 20).isActive = true
        } else {
            userNameButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        }

        userNameButton.translatesAutoresizingMaskIntoConstraints = false
        userNameButton.setTitleColor(ColorName.ebonyClay.color, for: .normal)
        userNameButton.titleLabel?.font = .systemFont(ofSize: 24, weight: .semibold)
        userNameButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true

        view.addSubview(repositoryNameLabel)
        repositoryNameLabel.translatesAutoresizingMaskIntoConstraints = false
        repositoryNameLabel.textColor = .gray
        repositoryNameLabel.font = .systemFont(ofSize: 18, weight: .medium)
        repositoryNameLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        repositoryNameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        repositoryNameLabel.topAnchor.constraint(equalTo: userNameButton.bottomAnchor, constant: 2).isActive = true

        view.addSubview(forkLabel)
        forkLabel.translatesAutoresizingMaskIntoConstraints = false
        forkLabel.textColor = ColorName.ebonyClay.color
        forkLabel.font = .systemFont(ofSize: 18, weight: .medium)
        forkLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        forkLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        forkLabel.topAnchor.constraint(equalTo: repositoryNameLabel.bottomAnchor, constant: 30).isActive = true

        view.addSubview(forksTableVIew)
        forksTableVIew.translatesAutoresizingMaskIntoConstraints = false
        forksTableVIew.backgroundColor = .white
        forksTableVIew.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        forksTableVIew.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        forksTableVIew.topAnchor.constraint(equalTo: forkLabel.bottomAnchor).isActive = true
        forksTableVIew.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        forksTableVIew.tableFooterView = UIView()
        forksTableVIew.separatorStyle = .none

        forksTableVIew.register(
            UINib(nibName: "ForkCell", bundle: nil),
            forCellReuseIdentifier: ForkCell.identifier
        )

        forksTableVIew.register(LoadingCell.self, forCellReuseIdentifier: LoadingCell.identifier)

        forksTableVIew.dataSource = self
        forksTableVIew.delegate = self
    }

    private func setupActions() {
        userNameButton.rx
            .tap
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel.map {  self?.router?.routeToRepositories(userName: $0.userName) }
            })
            .disposed(by: disposeBag)
    }

    private func setupNavigation() {
        navigationController?.navigationBar.tintColor = ColorName.ebonyClay.color
        navigationController?.navigationBar.barTintColor = .white
    }
}

extension DetailPageController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = viewModel?.items.count, count > 0 else {
            return 0
        }
        return needUpdate ? count + 1 : count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard !isLoadingCell(indexPath: indexPath) else {
            interactor?.fetchNextForksList(page: (viewModel?.currentPage ?? 0) + 1)
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadingCell.identifier, for: indexPath)
            if let cell = cell as? LoadingCell {
                cell.startAnimating()
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: ForkCell.identifier, for: indexPath)

        if let cell = cell as? ForkCell,
            let item = viewModel?.items[indexPath.row] {
            cell.set(item: item)
        }

        return cell
    }

    private func isLoadingCell(indexPath: IndexPath) -> Bool {
        indexPath.row >= (viewModel?.items.count ?? 0)
    }
}

extension DetailPageController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = viewModel?.items[indexPath.row] else { return }
        router?.routeToRepositories(userName: item.loginName)
    }
}

extension DetailPageController: DetailPageDisplayLogic {
    func reload(viewModel: ForksModel.ViewModel) {
        userNameButton.setTitle(viewModel.userName, for: .normal)
        repositoryNameLabel.text = viewModel.repositoryname
        forkLabel.text = "Forks"

        self.viewModel = viewModel
        needUpdate = viewModel.hasMore
        forksTableVIew.reloadData()
    }

    func next(viewModel: ForksModel.ViewModel) {
        let newItemCount = viewModel.items.count - (self.viewModel?.items.count ?? 0)
        guard newItemCount > 0 else {
            needUpdate = false
            return
        }
        self.viewModel = viewModel
        let itemsIndexPath = (0...newItemCount - 1).map { IndexPath(row: (viewModel.currentPage - 1) * 10 + $0, section: 0) }
        forksTableVIew.insertRows(at: itemsIndexPath, with: .none)
        needUpdate = viewModel.hasMore
        if !viewModel.hasMore {
            forksTableVIew.reloadData()
        }

        forksTableVIew.setContentOffset(forksTableVIew.contentOffset, animated: false)
    }

    func error(message: String) {
        messageToast(message: message, color: ColorName.roseRed.color)
    }
}
