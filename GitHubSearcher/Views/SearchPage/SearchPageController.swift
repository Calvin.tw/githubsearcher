//
//  SearchViewController.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 09.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit

protocol SearchPageDisplayLogic: UIViewController {
    func reload(viewModel: RepositoriesSearchModel.ViewModel)
    func next(viewModel: RepositoriesSearchModel.ViewModel)
    func error(message: String)
}

final class SearchPageController: UIViewController {

    // Views

    private lazy var resultTableView = UITableView(frame: .zero, style: .plain)
    private lazy var searchBar = UISearchBar(frame: .zero)

    // Flow

    var interactor: SearchPageBusinessLogic?

    var router: SearchPageRoutingLogic?

    var viewModel: RepositoriesSearchModel.ViewModel?

    private var needUpdate = true

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupSearchBar()
        setupNavigation()
    }

    private func setupViews() {
        view.addSubview(resultTableView)

        resultTableView.translatesAutoresizingMaskIntoConstraints = false
        resultTableView.backgroundColor = ColorName.gallery.color
        resultTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        resultTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        resultTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        resultTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        resultTableView.tableFooterView = UIView()
        resultTableView.separatorStyle = .none

        resultTableView.register(
            UINib(nibName: "SearchResultCell", bundle: nil),
            forCellReuseIdentifier: SearchResultCell.identifier
        )
        resultTableView.register(LoadingCell.self, forCellReuseIdentifier: LoadingCell.identifier)

        resultTableView.dataSource = self
        resultTableView.delegate = self
    }

    private func setupSearchBar() {
        searchBar.delegate = self
        searchBar.placeholder = "Search Repositories"
        navigationItem.titleView = searchBar
        definesPresentationContext = true
    }

    private func setupNavigation() {
        navigationController?.navigationBar.tintColor = ColorName.ebonyClay.color
        navigationController?.navigationBar.barTintColor = .white
    }
}

extension SearchPageController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let request = RepositoriesSearchModel.Request(query: searchBar.text, pageSize: 10, page: 1)
        interactor?.queryRepositories(request: request)
    }

}

extension SearchPageController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = viewModel?.items.count, count > 0 else {
            return 0
        }
        return needUpdate ? count + 1 : count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard !isLoadingCell(indexPath: indexPath) else {
            let request = RepositoriesSearchModel.Request(
                query: searchBar.text,
                pageSize: 10,
                page: (viewModel?.currentPage ?? 0) + 1
            )
            interactor?.queryNextRepositories(request: request)
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadingCell.identifier, for: indexPath)
            if let cell = cell as? LoadingCell {
                cell.startAnimating()
                cell.contentView.backgroundColor = ColorName.gallery.color
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultCell.identifier, for: indexPath)

        if let cell = cell as? SearchResultCell,
            let item = viewModel?.items[indexPath.row] {
            cell.set(item: item)
        }

        return cell
    }

    private func isLoadingCell(indexPath: IndexPath) -> Bool {
        indexPath.row >= (viewModel?.items.count ?? 0)
    }
}

extension SearchPageController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        140
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = viewModel?.items[indexPath.row], item.hasForks else { return }
        router?.routeToDetail(userName: item.loginName, repositoryName: item.repositoryName)
    }
}

extension SearchPageController: SearchPageDisplayLogic {
    func next(viewModel: RepositoriesSearchModel.ViewModel) {
        let newItemCount = viewModel.items.count - (self.viewModel?.items.count ?? 0)
        guard newItemCount > 0 else {
            needUpdate = false
            return
        }
        self.viewModel = viewModel
        let itemsIndexPath = (0...newItemCount - 1).map { IndexPath(row: (viewModel.currentPage - 1) * 10 + $0, section: 0) }
        resultTableView.insertRows(at: itemsIndexPath, with: .none)
        resultTableView.setContentOffset(resultTableView.contentOffset, animated: false)
        needUpdate = viewModel.hasMore
        if !viewModel.hasMore {
            resultTableView.reloadData()
        }

        resultTableView.setContentOffset(resultTableView.contentOffset, animated: false)
    }

    func reload(viewModel: RepositoriesSearchModel.ViewModel) {
        guard !viewModel.items.isEmpty else {
            messageToast(message: "Nothing find, please try other key words", color: ColorName.roseRed.color)
            return
        }
        self.viewModel = viewModel
        needUpdate = viewModel.hasMore
        resultTableView.reloadData()
    }

    func error(message: String) {
        messageToast(message: message, color: ColorName.roseRed.color)
    }
}
