//
//  SearchPageAssembly.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 10.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit
import Swinject

class SearchPageAssembly: Assembly {
    func assemble(container: Container) {
        container.register(SearchPageController.self) { resolver in
            let viewController = SearchPageController()
            var interactor = resolver.resolve(SearchPageBusinessLogic.self)
            var presenter = resolver.resolve(SearchPagePresentationLogic.self)
            var router = resolver.resolve(SearchPageRoutingLogic.self)
            presenter?.viewController = viewController
            interactor?.presenter = presenter
            router?.viewController = viewController
            viewController.interactor = interactor
            viewController.router = router
            return viewController
        }
        .inObjectScope(.container)

        container.register(SearchPageInteractor.Context.self) { resolver in
            .init(apiWorker: resolver.resolveAPIWorker())
        }

        container.register(SearchPageBusinessLogic.self) { resolver in
            SearchPageInteractor(context: resolver.resolve(SearchPageInteractor.Context.self)!)
        }

        container.register(SearchPagePresentationLogic.self) { _ in
            SearchPagePresenter()
        }

        container.register(SearchPageRoutingLogic.self) { resolver in
            SearchPageRouter(resolver: resolver)
        }
    }
}

extension Resolver {
    func resolveSearchPageController() -> SearchPageController {
        resolve(SearchPageController.self)!
    }
}
