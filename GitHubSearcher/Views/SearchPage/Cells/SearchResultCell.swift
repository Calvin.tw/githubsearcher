//
//  SearchResultCell.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 10.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit
import Kingfisher

class SearchResultCell: UITableViewCell {
    static let identifier = "SearchResultCell"

    @IBOutlet private weak var repositoryNameLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var numberOfForksLabel: UILabel!
    @IBOutlet private weak var forksLabel: UILabel!
    @IBOutlet private weak var numberOfWatchersLabel: UILabel!
    @IBOutlet private weak var watchersLabel: UILabel!
    @IBOutlet private weak var indicator: UIImageView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 14
        containerView.layer.masksToBounds = false
        containerView.layer.shadowColor = UIColor.gray.cgColor
        containerView.layer.shadowOpacity = 0.1
        containerView.layer.shadowOffset = .zero
        containerView.layer.shadowRadius = 10

        repositoryNameLabel.textColor = ColorName.ebonyClay.color
        descriptionLabel.textColor = .gray
        numberOfForksLabel.textColor = ColorName.bondiBlue.color
        forksLabel.textColor = ColorName.bondiBlue.color
        numberOfWatchersLabel.textColor = ColorName.bondiBlue.color
        watchersLabel.textColor = ColorName.bondiBlue.color
        contentView.backgroundColor = ColorName.gallery.color
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
    }

    override func prepareForReuse() {
        repositoryNameLabel.text = nil
        descriptionLabel.text = nil
        numberOfForksLabel.text = nil
        numberOfWatchersLabel.text = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
    }

    func set(item: SearchPageItem) {
        repositoryNameLabel.text = item.repositoryName
        descriptionLabel.text = item.description
        numberOfForksLabel.text = "\(item.numberOfForks)"
        numberOfWatchersLabel.text = "\(item.numberOfWatchers)"
        avatarImageView.kf.setImage(with: item.resource)
        indicator.isHidden = !item.hasForks
    }
}
