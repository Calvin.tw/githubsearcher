//
//  UserRepositoriesModel.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation

struct UserRepositoriesModel {
    struct Request {
        let user: String
        let page: Int
        let pageSize: Int
    }

    struct Response {
        let page: Int
        let items: [RepositoryEntity]
    }

    struct ViewModel {
        let userName: String
        let currentPage: Int
        let items: [UserRepositoryItem]
        let hasMore: Bool
    }
}

protocol UserRepositoryItem {
    var repositoryName: String { get }
}
