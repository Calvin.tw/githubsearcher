//
//  Model+APIRequest.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation

extension RepositoriesSearchModel.Request: APIRequest {
    func parameters() -> [String : Any]? {
        var parameters = [String : Any]()
        parameters["q"] = query
        parameters["page"] = page
        parameters["per_page"] = pageSize
        return parameters
    }
}

extension ForksModel.Request: APIRequest {
    func parameters() -> [String : Any]? {
        var parameters = [String : Any]()
        parameters["page"] = page
        parameters["per_page"] = pageSize
        return parameters
    }
}

extension UserRepositoriesModel.Request: APIRequest {
    func parameters() -> [String : Any]? {
        var parameters = [String : Any]()
        parameters["page"] = page
        parameters["per_page"] = pageSize
        return parameters
    }
}
