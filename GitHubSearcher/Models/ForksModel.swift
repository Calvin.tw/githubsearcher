//
//  ForksModel.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation
import Kingfisher

struct ForksModel {
    struct Request {
        let user: String
        let repo: String
        let page: Int
        let pageSize: Int
    }

    struct Response {
        let page: Int
        let items: [RepositoryEntity]
    }

    struct ViewModel {
        let userName: String
        let repositoryname: String
        let currentPage: Int
        let items: [ForkItem]
        let hasMore: Bool
    }
}

protocol ForkItem {
    var avatarUrl: String { get }
    var loginName: String { get }
    var resource: Resource? { get }
}
