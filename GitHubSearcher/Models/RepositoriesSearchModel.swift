//
//  RepositoriesSearchModel.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation
import Kingfisher

struct RepositoriesSearchModel {
    struct Request {
        let query: String?
        let pageSize: Int
        let page: Int
    }

    struct Response: Codable {
        let totalCount: Int
        let items: [RepositoryEntity]

        enum CodingKeys: String, CodingKey {
            case totalCount = "total_count"
            case items
        }
    }

    struct ViewModel {
        let items: [SearchPageItem]
        let currentPage: Int
        let hasMore: Bool
    }
}

protocol SearchPageItem {
    var loginName: String { get }
    var avatarUrl: String { get }
    var repositoryName: String { get }
    var description: String? { get }
    var numberOfForks: Int { get }
    var numberOfWatchers: Int { get }
    var hasForks: Bool { get }
    var resource: Resource? { get }
}
