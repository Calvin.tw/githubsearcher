//
//  RepositoryEntity.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation

struct RepositoryEntity: Codable {
    let id: Int
    let name: String
    let description: String?
    let forks: Int
    let watchers: Int
    let owner: OwnerEntity

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case forks
        case watchers
        case owner
    }
}
