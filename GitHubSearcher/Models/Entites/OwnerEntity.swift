//
//  OwnerEntity.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation

struct OwnerEntity: Codable {
    let id: Int
    let name: String
    let avatarUrl: String

    enum CodingKeys: String, CodingKey {
        case id
        case name = "login"
        case avatarUrl = "avatar_url"
    }
}
