//
//  AppDelegate.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 07.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

// swiftlint:disable all

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let resolver: Resolver

    override init() {
        resolver = ResolverFactory.make().resolver
        super.init()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        if NSClassFromString("XCTest") != nil { return true }

        window = UIWindow(frame: UIScreen.main.bounds)
        let navigation = UINavigationController(rootViewController: resolver.resolveSearchPageController())

        window?.rootViewController = navigation
        window?.makeKeyAndVisible()

        return true
    }
}

