//
//  APIWorker.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import RxCocoa
import RxSwift
import Moya

protocol APIWorkingLogic {
    func queryRepositories(request: RepositoriesSearchModel.Request) -> Single<RepositoriesSearchModel.Response>
    func requestUserRepositories(request: UserRepositoriesModel.Request) -> Single<UserRepositoriesModel.Response>
    func requestForksRepositories(request: ForksModel.Request) -> Single<ForksModel.Response>
}

final class APIWorker: APIWorkingLogic {
    let provider: MoyaProvider<APIType>

    init(provider: MoyaProvider<APIType>) {
        self.provider = provider
    }

    func queryRepositories(request: RepositoriesSearchModel.Request) -> Single<RepositoriesSearchModel.Response> {
        provider.rx
            .request(.search(request))
            .map(RepositoriesSearchModel.Response.self)
    }

    func requestUserRepositories(request: UserRepositoriesModel.Request) -> Single<UserRepositoriesModel.Response> {
        provider.rx
            .request(.user(request))
            .map([RepositoryEntity].self)
            .map { UserRepositoriesModel.Response(page: request.page, items: $0) }
    }

    func requestForksRepositories(request: ForksModel.Request) -> Single<ForksModel.Response> {
        provider.rx
            .request(.forks(request))
            .map([RepositoryEntity].self)
            .map { ForksModel.Response(page: request.page, items: $0) }
    }
}
