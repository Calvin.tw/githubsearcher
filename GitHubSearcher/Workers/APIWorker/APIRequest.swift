//
//  APIRequest.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation

protocol APIRequest {
    func parameters() -> [String : Any]?
}
