//
//  APIWorkerAssembly.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Swinject
import Moya

class APIWorkerAssembly: Assembly {
    func assemble(container: Container) {
        container.register(MoyaProvider<APIType>.self) { _ in
            MoyaProvider<APIType>(
            stubClosure: MoyaProvider.neverStub,
            plugins: [NetworkLoggerPlugin()])
        }

        container.register(APIWorkingLogic.self) { resolver -> APIWorkingLogic in
            APIWorker(provider: resolver.resolve(MoyaProvider<APIType>.self)!)
        }
    }

    func JSONResponseDataFormatter(_ data: Data) -> Data {
        do {
            let dataAsJSON = try JSONSerialization.jsonObject(with: data)
            let prettyData = try JSONSerialization.data(withJSONObject:dataAsJSON , options: .prettyPrinted)
            return prettyData
        } catch {
            return data
        }
    }
}

extension Resolver {
    func resolveAPIWorker() -> APIWorkingLogic {
        resolve(APIWorkingLogic.self)!
    }
}
