//
//  APIType.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Moya

enum APIType: TargetType {
    private var base: String { return "https://api.github.com/" }

    case search(RepositoriesSearchModel.Request)
    case user(UserRepositoriesModel.Request)
    case forks(ForksModel.Request)

    /// The target's base `URL`.
    var baseURL: URL {
        return URL(string: base)!
    }

    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String {
        switch self {
        case .search:
            return "search/repositories"
        case .user(let request):
            return "users/\(request.user)/repos"
        case .forks(let request):
            return "repos/\(request.user)/\(request.repo)/forks"
        }
    }

    /// The HTTP method used in the request.
    var method: Moya.Method {
        .get
    }

    /// Provides stub data for use in testing.
    var sampleData: Data {
        switch self {
        case .search:
            if let data = stubData(fileName: "Search", ofType: "json") { return data }
        case .user:
            if let data = stubData(fileName: "User", ofType: "json") { return data }
        case .forks:
            if let data = stubData(fileName: "Forks", ofType: "json") { return data }
        }
        return Data()
    }

    /// The type of HTTP task to be performed.
    public var task: Task {
        switch self {
        case .search(let request):
            if let parameters = request.parameters() {
                return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
            } else {
                return .requestPlain
            }
        case .user(let request):
            if let parameters = request.parameters() {
                return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
            } else {
                return .requestPlain
            }
        case .forks(let request):
            if let parameters = request.parameters() {
                return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
            } else {
                return .requestPlain
            }
        }
    }

    /// The type of validation to perform on the request. Default is `.none`.
    var validationType: ValidationType {
        return .customCodes(Array(200...299))
    }

    /// The headers to be used in the request.
    var headers: [String: String]? {
        nil
    }

    func stubData(fileName: String, ofType: String) -> Data? {
        do {
            if let path = Bundle.main.path(forResource: fileName, ofType: ofType) {
                if let data = try String(contentsOfFile: path).data(using: .utf8) {
                    return data
                }
            }
            return nil
        } catch {
            return nil
        }
    }
}
