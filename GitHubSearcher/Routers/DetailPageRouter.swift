//
//  DetailPageRouter.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 10.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Swinject

protocol DetailPageRoutingLogic {
    var viewController: UIViewController? { get set }
    func routeToRepositories(userName: String)
}

final class DetailPageRouter: DetailPageRoutingLogic {
    let resolver: Resolver

    weak var viewController: UIViewController?

    init(resolver: Resolver) {
        self.resolver = resolver
    }

    func routeToRepositories(userName: String) {
        let data = RepositoriesPage.InputData(userName: userName)
        let next = resolver.resolveRepositoriesPageController(inputData: data)
        next.modalPresentationStyle = .pageSheet
        viewController?.present(next, animated: true, completion: nil)
    }
}
