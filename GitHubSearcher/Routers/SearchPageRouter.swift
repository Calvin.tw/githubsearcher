//
//  SearchPageRouter.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 10.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Swinject

protocol SearchPageRoutingLogic {
    var viewController: UIViewController? { get set }
    func routeToDetail(userName: String, repositoryName: String)
}

final class SearchPageRouter: SearchPageRoutingLogic {
    let resolver: Resolver

    weak var viewController: UIViewController?

    init(resolver: Resolver) {
        self.resolver = resolver
    }

    func routeToDetail(userName: String, repositoryName: String) {
        let data = DetailPage.InputData(userName: userName, repositoryName: repositoryName)
        let next = resolver.resolveDetailPageController(inputData: data)
        viewController?.navigationController?.pushViewController(next, animated: true)
    }
}
