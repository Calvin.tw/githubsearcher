//
//  DetailPageInteractor.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 09.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum DetailPage {
    struct InputData {
        let userName: String
        let repositoryName: String
    }
}

protocol DetailPageBusinessLogic {
    var presenter: DetailPagePresentationLogic? { get set }

    func fetchForksList()
    func fetchNextForksList(page: Int)
}

final class DetailPageInteractor: DetailPageBusinessLogic {
    struct Context {
        let apiWorker: APIWorkingLogic
    }

    var presenter: DetailPagePresentationLogic?
    private let context: Context
    private let inputData: DetailPage.InputData
    private let disposeBag = DisposeBag()

    init(context: Context, inputData: DetailPage.InputData) {
        self.context = context
        self.inputData = inputData
    }

    func fetchForksList() {
        let request = ForksModel.Request(
            user: inputData.userName,
            repo: inputData.repositoryName,
            page: 1,
            pageSize: 10
        )
        context.apiWorker
            .requestForksRepositories(request: request)
            .subscribe(
                onSuccess: { [weak self] in
                    self?.presenter?.reload(response: $0)
                },
                onError: { [weak self] in
                    self?.presenter?.presnt(error: $0)
                }
            )
            .disposed(by: disposeBag)
    }

    func fetchNextForksList(page: Int) {
        let request = ForksModel.Request(
            user: inputData.userName,
            repo: inputData.repositoryName,
            page: page,
            pageSize: 10
        )
        context.apiWorker
            .requestForksRepositories(request: request)
            .subscribe(
                onSuccess: { [weak self] in
                    self?.presenter?.present(response: $0, page: page)
                },
                onError: { [weak self] in
                    self?.presenter?.presnt(error: $0)
                }
            )
            .disposed(by: disposeBag)
    }
}
