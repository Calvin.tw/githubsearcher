//
//  SearchPageInteractor.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 09.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol SearchPageBusinessLogic {
    var presenter: SearchPagePresentationLogic? { get set }

    func queryRepositories(request: RepositoriesSearchModel.Request)
    func queryNextRepositories(request: RepositoriesSearchModel.Request)
}

final class SearchPageInteractor: SearchPageBusinessLogic {
    struct Context {
        let apiWorker: APIWorkingLogic
    }

    var presenter: SearchPagePresentationLogic?
    private let context: Context
    private let disposeBag = DisposeBag()

    init(context: Context) {
        self.context = context
    }

    func queryRepositories(request: RepositoriesSearchModel.Request) {
        context.apiWorker
            .queryRepositories(request: request)
            .subscribe(
                onSuccess: { [weak self] in
                    self?.presenter?.reload(response: $0)
                },
                onError: { [weak self] in
                    self?.presenter?.presnt(error: $0)
                }
            )
            .disposed(by: disposeBag)
    }

    func queryNextRepositories(request: RepositoriesSearchModel.Request) {
        context.apiWorker
            .queryRepositories(request: request)
            .subscribe(
                onSuccess: { [weak self] in
                    self?.presenter?.present(response: $0, page: request.page)
                },
                onError: { [weak self] in
                    self?.presenter?.presnt(error: $0)
                }
            )
            .disposed(by: disposeBag)
    }
}
