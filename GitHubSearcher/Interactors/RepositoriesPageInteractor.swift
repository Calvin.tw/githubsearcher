//
//  RepositoriesPageInteractor.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 09.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum RepositoriesPage {
    struct InputData {
        let userName: String
    }
}

protocol RepositoriesPageBusinessLogic {
    var presenter: RepositoriesPagePresentationLogic? { get set }

    func fetchUserRepositories()
    func fetchNextUserRepositories(page: Int)
}

final class RepositoriesPageInteractor: RepositoriesPageBusinessLogic {
    struct Context {
        let apiWorker: APIWorkingLogic
    }

    var presenter: RepositoriesPagePresentationLogic?
    private let context: Context
    private let inputData: RepositoriesPage.InputData
    private let disposeBag = DisposeBag()

    init(context: Context, inputData: RepositoriesPage.InputData) {
        self.context = context
        self.inputData = inputData
    }

    func fetchUserRepositories() {
        let request = UserRepositoriesModel.Request(user: inputData.userName, page: 1, pageSize: 10)
        context.apiWorker
            .requestUserRepositories(request: request)
            .subscribe(
                onSuccess: { [weak self] in
                    self?.presenter?.reload(response: $0)
                },
                onError: { [weak self] in
                    self?.presenter?.presnt(error: $0)
                }
            )
            .disposed(by: disposeBag)
    }

    func fetchNextUserRepositories(page: Int) {
        let request = UserRepositoriesModel.Request(user: inputData.userName, page: page, pageSize: 10)
        context.apiWorker
            .requestUserRepositories(request: request)
            .subscribe(
                onSuccess: { [weak self] in
                    self?.presenter?.present(response: $0, page: page)
                },
                onError: { [weak self] in
                    self?.presenter?.presnt(error: $0)
                }
            )
            .disposed(by: disposeBag)
    }

}
