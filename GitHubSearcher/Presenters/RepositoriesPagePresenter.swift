//
//  RepositoriesPagePresenter.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 09.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Moya

protocol RepositoriesPagePresentationLogic {
    var viewController: RepositoriesPageDisplayLogic? { get set }

    func reload(response: UserRepositoriesModel.Response)
    func present(response: UserRepositoriesModel.Response, page: Int)
    func presnt(error: Error)
}

final class RepositoriesPagePresenter: RepositoriesPagePresentationLogic {
    weak var viewController: RepositoriesPageDisplayLogic?

    private let inputData: RepositoriesPage.InputData

    private var items: [UserRepositoryItem] = []

    init(inputData: RepositoriesPage.InputData) {
        self.inputData = inputData
    }

    func reload(response: UserRepositoriesModel.Response) {
        items = response.items
        let viewModel = UserRepositoriesModel.ViewModel(
            userName: inputData.userName,
            currentPage: 1,
            items: items,
            hasMore: !response.items.isEmpty
        )
        viewController?.reload(viewModel: viewModel)
    }

    func present(response: UserRepositoriesModel.Response, page: Int) {
        items.append(contentsOf: response.items)
        let viewModel = UserRepositoriesModel.ViewModel(
            userName: inputData.userName,
            currentPage: page,
            items: items,
            hasMore: !response.items.isEmpty
        )
        viewController?.next(viewModel: viewModel)
    }

    func presnt(error: Error) {
        if let error = error as? MoyaError {
            error.errorDescription.map { viewController?.error(message: $0) }
        } else {
            viewController?.error(message: "Something wrong, please try again later")
        }
    }
}

extension RepositoryEntity: UserRepositoryItem {}
