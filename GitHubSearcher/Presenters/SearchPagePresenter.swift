//
//  SearchPagePresenter.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 09.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation
import Moya
import Kingfisher

protocol SearchPagePresentationLogic {
    var viewController: SearchPageDisplayLogic? { get set }

    func reload(response: RepositoriesSearchModel.Response)
    func present(response: RepositoriesSearchModel.Response, page: Int)
    func presnt(error: Error)
}

final class SearchPagePresenter: SearchPagePresentationLogic {
    weak var viewController: SearchPageDisplayLogic?

    private var items: [SearchPageItem] = []

    func reload(response: RepositoriesSearchModel.Response) {
        items = response.items
        let viewModel = RepositoriesSearchModel.ViewModel(
            items: items,
            currentPage: 1,
            hasMore: response.totalCount != items.count
        )
        viewController?.reload(viewModel: viewModel)
    }

    func present(response: RepositoriesSearchModel.Response, page: Int) {
        items.append(contentsOf: response.items)
        let viewModel = RepositoriesSearchModel.ViewModel(
            items: items,
            currentPage: page,
            hasMore: response.totalCount != items.count
        )
        viewController?.next(viewModel: viewModel)
    }

    func presnt(error: Error) {
        if let error = error as? MoyaError {
            error.errorDescription.map { viewController?.error(message: $0) }
        } else {
            viewController?.error(message: "Something wrong, please try again later")
        }
    }
}

extension RepositoryEntity: SearchPageItem {
    var avatarUrl: String { owner.avatarUrl }
    var repositoryName: String { name }
    var numberOfForks: Int { forks }
    var numberOfWatchers: Int { watchers }
    var hasForks: Bool { forks != 0 }
    var resource: Resource? { URL(string: avatarUrl).map { ImageResource(downloadURL: $0, cacheKey: "\(id)") } }
}
