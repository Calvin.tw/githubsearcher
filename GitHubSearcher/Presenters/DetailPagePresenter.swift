//
//  DetailPagePresenter.swift
//  GitHubSearcher
//
//  Created by Chang Wen-Lung on 09.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Moya

protocol DetailPagePresentationLogic {
    var viewController: DetailPageDisplayLogic? { get set }

    func reload(response: ForksModel.Response)
    func present(response: ForksModel.Response, page: Int)
    func presnt(error: Error)
}

final class DetailPagePresenter: DetailPagePresentationLogic {
    weak var viewController: DetailPageDisplayLogic?

    private let inputData: DetailPage.InputData

    private var items: [ForkItem] = []

    init(inputData: DetailPage.InputData) {
        self.inputData = inputData
    }

    func reload(response: ForksModel.Response) {
        items = response.items
        let viewModel = ForksModel.ViewModel(
            userName: inputData.userName,
            repositoryname: inputData.repositoryName,
            currentPage: 1,
            items: items,
            hasMore: !response.items.isEmpty && response.items.count == 10
        )
        viewController?.reload(viewModel: viewModel)
    }

    func present(response: ForksModel.Response, page: Int) {
        items.append(contentsOf: response.items)
        let viewModel = ForksModel.ViewModel(
            userName: inputData.userName,
            repositoryname: inputData.repositoryName,
            currentPage: page,
            items: items,
            hasMore: !response.items.isEmpty && response.items.count == 10
        )
        viewController?.next(viewModel: viewModel)
    }

    func presnt(error: Error) {
        if let error = error as? MoyaError {
            error.errorDescription.map { viewController?.error(message: $0) }
        } else {
            viewController?.error(message: "Something wrong, please try again later")
        }
    }
}

extension RepositoryEntity: ForkItem {
    var loginName: String { owner.name }
}
