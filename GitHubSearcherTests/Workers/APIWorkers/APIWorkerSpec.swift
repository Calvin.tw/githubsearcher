//
//  APIWorkerApec.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 08.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Quick
import Nimble
import RxTest
import RxCocoa
import RxSwift
import Moya

@testable import GitHubSearcher

final class APIWorkerSpec: QuickSpec {
    override func spec() {
        var worker: APIWorkingLogic!
        var disposeBag: DisposeBag!

        beforeEach {
            disposeBag = DisposeBag()
            let provider = MoyaProvider<APIType>(stubClosure: MoyaProvider.immediatelyStub)
            worker = APIWorker(provider: provider)
        }

        it("Should receive and parse query normally") {
            var response: RepositoriesSearchModel.Response?
            worker
                .queryRepositories(request: RepositoriesSearchModel.Request(query: "", pageSize: 10, page: 1))
                .subscribe(
                    onSuccess: { response = $0 },
                    onError: { _ in fail() }
                )
                .disposed(by: disposeBag)

            expect(response?.items.count).toEventually(equal(30))
        }

        it("Should receive and parse user normally") {
            var response: UserRepositoriesModel.Response?
            worker
                .requestUserRepositories(request: UserRepositoriesModel.Request(user: "", page: 1, pageSize: 30))
                .subscribe(
                    onSuccess: { response = $0 },
                    onError: { _ in fail() }
                )
                .disposed(by: disposeBag)

            expect(response?.items.count).toEventually(equal(11))
        }

        it("Should receive and parse forks normally") {
            var response: ForksModel.Response?
            worker
                .requestForksRepositories(request: ForksModel.Request(user: "", repo: "", page: 1, pageSize: 30))
                .subscribe(
                    onSuccess: { response = $0 },
                    onError: { _ in fail() }
                )
                .disposed(by: disposeBag)

            expect(response?.items.count).toEventually(equal(30))
        }
    }
}
