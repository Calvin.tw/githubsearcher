//
//  APIWorkerMock.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 09.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import RxCocoa
import RxSwift
import Moya

@testable import GitHubSearcher

final class APIWorkerMock: APIWorkingLogic {
    let provider = MoyaProvider<APIType>(stubClosure: MoyaProvider.immediatelyStub)

    func queryRepositories(request: RepositoriesSearchModel.Request) -> Single<RepositoriesSearchModel.Response> {
        provider.rx
            .request(.search(request))
            .map(RepositoriesSearchModel.Response.self)
    }

    func requestUserRepositories(request: UserRepositoriesModel.Request) -> Single<UserRepositoriesModel.Response> {
        provider.rx
            .request(.user(request))
            .map([RepositoryEntity].self)
            .map { UserRepositoriesModel.Response(page: 1, items: $0) }
    }

    func requestForksRepositories(request: ForksModel.Request) -> Single<ForksModel.Response> {
        provider.rx
            .request(.forks(request))
            .map([RepositoryEntity].self)
            .map { ForksModel.Response(page: 1, items: $0) }
    }
}
