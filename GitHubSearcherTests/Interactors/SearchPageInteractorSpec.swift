//
//  SearchPageInteractorSpec.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Quick
import Nimble

@testable import GitHubSearcher

final class SearchPageInteractorSpec: QuickSpec {
    override func spec() {
        var interactor: SearchPageBusinessLogic!
        var presenterMock: SearchPagePresenterMock!

        beforeEach {
            let worker = APIWorkerMock()
            interactor = SearchPageInteractor(context: SearchPageInteractor.Context(apiWorker: worker))
            presenterMock = SearchPagePresenterMock()
            interactor.presenter = presenterMock
        }

        it("Should query normally") {
            interactor.queryRepositories(request: RepositoriesSearchModel.Request(query: "", pageSize: 30, page: 1))

            expect(presenterMock.reloadCallsCount).to(equal(1))
            expect(presenterMock.reloadReceivedResponse?.items.count).to(equal(30))
        }

        it("Should query next page normally") {
            interactor.queryNextRepositories(request: RepositoriesSearchModel.Request(query: "", pageSize: 30, page: 1))

            expect(presenterMock.presentCallsCount).to(equal(1))
            expect(presenterMock.presentReceivedResponse?.items.count).to(equal(30))
        }
    }
}
