//
//  RepositoriesPageInteractorSpec.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Quick
import Nimble
import Fakery

@testable import GitHubSearcher

final class RepositoriesPageInteractorSpec: QuickSpec {
    override func spec() {
        var interactor: RepositoriesPageBusinessLogic!
        var presenterMock: RepositoriesPagePresenterMock!

        beforeEach {
            let worker = APIWorkerMock()
            interactor = RepositoriesPageInteractor(
                context: RepositoriesPageInteractor.Context(apiWorker: worker),
                inputData: RepositoriesPage.InputData(userName: Faker().name.name())
            )
            presenterMock = RepositoriesPagePresenterMock()
            interactor.presenter = presenterMock
        }

        it("Should query normally") {
            interactor.fetchUserRepositories()

            expect(presenterMock.reloadCallsCount).to(equal(1))
            expect(presenterMock.reloadReceivedResponse?.items.count).to(equal(11))
        }

        it("Should query next page normally") {
            interactor.fetchNextUserRepositories(page: 77)

            expect(presenterMock.presentCallsCount).to(equal(1))
            expect(presenterMock.presentReceivedResponse?.items.count).to(equal(11))
        }
    }
}
