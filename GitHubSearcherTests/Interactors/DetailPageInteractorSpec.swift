//
//  DetailPageInteractorSpec.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Quick
import Nimble
import Fakery

@testable import GitHubSearcher

final class DetailPageInteractorSpec: QuickSpec {
    override func spec() {
        var interactor: DetailPageBusinessLogic!
        var presenterMock: DetailPagePresenterMock!

        beforeEach {
            let worker = APIWorkerMock()
            interactor = DetailPageInteractor(
                context: DetailPageInteractor.Context(apiWorker: worker),
                inputData: DetailPage.InputData(
                    userName: Faker().name.name(),
                    repositoryName: Faker().name.name()
                )
            )
            presenterMock = DetailPagePresenterMock()
            interactor.presenter = presenterMock
        }

        it("Should query normally") {
            interactor.fetchForksList()

            expect(presenterMock.reloadCallsCount).to(equal(1))
            expect(presenterMock.reloadReceivedResponse?.items.count).to(equal(30))
        }

        it("Should query next page normally") {
            interactor.fetchNextForksList(page: 10)

            expect(presenterMock.presentCallsCount).to(equal(1))
            expect(presenterMock.presentReceivedResponse?.items.count).to(equal(30))
        }
    }
}
