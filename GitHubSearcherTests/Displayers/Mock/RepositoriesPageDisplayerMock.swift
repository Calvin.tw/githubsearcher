//
//  RepositoriesPageDisplayerMock.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit

@testable import GitHubSearcher

class RepositoriesPageDisplayerMock: UIViewController, RepositoriesPageDisplayLogic {
    var reloadCallsCount = 0
    var reloadCalled: Bool { reloadCallsCount > 0 }
    var reloadReceivedViewModel: UserRepositoriesModel.ViewModel?
    var reloadClosure: ((UserRepositoriesModel.ViewModel) -> Void)?
    func reload(viewModel: UserRepositoriesModel.ViewModel) {
        reloadCallsCount += 1
        reloadReceivedViewModel = viewModel
        reloadClosure?(viewModel)
    }

    var nextCallsCount = 0
    var nextCalled: Bool { nextCallsCount > 0 }
    var nextReceivedViewModel: UserRepositoriesModel.ViewModel?
    var nextClosure: ((UserRepositoriesModel.ViewModel) -> Void)?
    func next(viewModel: UserRepositoriesModel.ViewModel) {
        nextCallsCount += 1
        nextReceivedViewModel = viewModel
        nextClosure?(viewModel)
    }

    var errorCallsCount = 0
    var errorCalled: Bool { errorCallsCount > 0 }
    var errorReceivedMessage: String?
    var errorClosure: ((String) -> Void)?
    func error(message: String) {
        errorCallsCount += 1
        errorReceivedMessage = message
        errorClosure?(message)
    }
}
