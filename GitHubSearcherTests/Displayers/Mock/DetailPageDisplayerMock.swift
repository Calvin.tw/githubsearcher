//
//  DetailPageDisplayerMock.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import UIKit

@testable import GitHubSearcher

class DetailPageDisplayerMock: UIViewController, DetailPageDisplayLogic {
    var reloadCallsCount = 0
    var reloadCalled: Bool { reloadCallsCount > 0 }
    var reloadReceivedViewModel: ForksModel.ViewModel?
    var reloadClosure: ((ForksModel.ViewModel) -> Void)?
    func reload(viewModel: ForksModel.ViewModel) {
        reloadCallsCount += 1
        reloadReceivedViewModel = viewModel
        reloadClosure?(viewModel)
    }

    var nextCallsCount = 0
    var nextCalled: Bool { nextCallsCount > 0 }
    var nextReceivedViewModel: ForksModel.ViewModel?
    var nextClosure: ((ForksModel.ViewModel) -> Void)?
    func next(viewModel: ForksModel.ViewModel) {
        nextCallsCount += 1
        nextReceivedViewModel = viewModel
        nextClosure?(viewModel)
    }

    var errorCallsCount = 0
    var errorCalled: Bool { errorCallsCount > 0 }
    var errorReceivedMessage: String?
    var errorClosure: ((String) -> Void)?
    func error(message: String) {
        errorCallsCount += 1
        errorReceivedMessage = message
        errorClosure?(message)
    }
}
