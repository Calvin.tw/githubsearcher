//
//  SearchPagePresenterMock.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation

@testable import GitHubSearcher

final class SearchPagePresenterMock: SearchPagePresentationLogic {
    var viewController: SearchPageDisplayLogic?

    var reloadCallsCount = 0
    var reloadCalled: Bool { reloadCallsCount > 0 }
    var reloadReceivedResponse: RepositoriesSearchModel.Response?
    var reloadClosure: ((RepositoriesSearchModel.Response) -> Void)?
    func reload(response: RepositoriesSearchModel.Response) {
        reloadCallsCount += 1
        reloadReceivedResponse = response
        reloadClosure?(response)
    }

    var presentCallsCount = 0
    var presentCalled: Bool { presentCallsCount > 0 }
    var presentReceivedResponse: RepositoriesSearchModel.Response?
    var presentReceivedPage: Int?
    var presenteClosure: ((RepositoriesSearchModel.Response, Int) -> Void)?
    func present(response: RepositoriesSearchModel.Response, page: Int) {
        presentCallsCount += 1
        presentReceivedResponse = response
        presentReceivedPage = page
        presenteClosure?(response, page)
    }

    var errorCallsCount = 0
    var errorCalled: Bool { errorCallsCount > 0 }
    var errorReceivedResponse: Error?
    var errorClosure: ((Error) -> Void)?
    func presnt(error: Error) {
        errorCallsCount += 1
        errorReceivedResponse = error
        errorClosure?(error)
    }
}
