//
//  RepositoriesPagePresenterMock.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation

@testable import GitHubSearcher

final class RepositoriesPagePresenterMock: RepositoriesPagePresentationLogic {
    var viewController: RepositoriesPageDisplayLogic?

    var reloadCallsCount = 0
    var reloadCalled: Bool { reloadCallsCount > 0 }
    var reloadReceivedResponse: UserRepositoriesModel.Response?
    var reloadClosure: ((UserRepositoriesModel.Response) -> Void)?
    func reload(response: UserRepositoriesModel.Response) {
        reloadCallsCount += 1
        reloadReceivedResponse = response
        reloadClosure?(response)
    }

    var presentCallsCount = 0
    var presentCalled: Bool { presentCallsCount > 0 }
    var presentReceivedResponse: UserRepositoriesModel.Response?
    var presentReceivedPage: Int?
    var presenteClosure: ((UserRepositoriesModel.Response, Int) -> Void)?
    func present(response: UserRepositoriesModel.Response, page: Int) {
        presentCallsCount += 1
        presentReceivedResponse = response
        presentReceivedPage = page
        presenteClosure?(response, page)
    }

    var errorCallsCount = 0
    var errorCalled: Bool { errorCallsCount > 0 }
    var errorReceivedResponse: Error?
    var errorClosure: ((Error) -> Void)?
    func presnt(error: Error) {
        errorCallsCount += 1
        errorReceivedResponse = error
        errorClosure?(error)
    }
}
