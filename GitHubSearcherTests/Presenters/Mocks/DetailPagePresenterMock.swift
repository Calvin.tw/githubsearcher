//
//  DetailPagePresenterMock.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Foundation

@testable import GitHubSearcher

final class DetailPagePresenterMock: DetailPagePresentationLogic {
    var viewController: DetailPageDisplayLogic?

    var reloadCallsCount = 0
    var reloadCalled: Bool { reloadCallsCount > 0 }
    var reloadReceivedResponse: ForksModel.Response?
    var reloadClosure: ((ForksModel.Response) -> Void)?
    func reload(response: ForksModel.Response) {
        reloadCallsCount += 1
        reloadReceivedResponse = response
        reloadClosure?(response)
    }

    var presentCallsCount = 0
    var presentCalled: Bool { presentCallsCount > 0 }
    var presentReceivedResponse: ForksModel.Response?
    var presentReceivedPage: Int?
    var presenteClosure: ((ForksModel.Response, Int) -> Void)?
    func present(response: ForksModel.Response, page: Int) {
        presentCallsCount += 1
        presentReceivedResponse = response
        presentReceivedPage = page
        presenteClosure?(response, page)
    }

    var errorCallsCount = 0
    var errorCalled: Bool { errorCallsCount > 0 }
    var errorReceivedResponse: Error?
    var errorClosure: ((Error) -> Void)?
    func presnt(error: Error) {
        errorCallsCount += 1
        errorReceivedResponse = error
        errorClosure?(error)
    }
}
