//
//  SearchPagePresenterSpec.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Quick
import Nimble
import Fakery

@testable import GitHubSearcher

final class SearchPagePresenterSpec: QuickSpec {
    override func spec() {
        var presenter: SearchPagePresentationLogic!
        var displayerMock: SearchPageDisplayerMock!

        beforeEach {
            presenter = SearchPagePresenter()
            displayerMock = SearchPageDisplayerMock()
            presenter.viewController = displayerMock
        }

        it("Should reload normally") {
            presenter.reload(response: RepositoriesSearchModel.Response(totalCount: Faker().number.randomInt(), items: []))
            expect(displayerMock.reloadCallsCount).to(equal(1))
            expect(displayerMock.reloadReceivedViewModel?.items.count).to(equal(0))
        }

        it("Should present next normally") {
            presenter.present(
                response: RepositoriesSearchModel.Response(
                    totalCount: Faker().number.randomInt(),
                    items: []),
                page: Faker().number.randomInt()
            )

            expect(displayerMock.nextCallsCount).to(equal(1))
            expect(displayerMock.nextReceivedViewModel?.items.count).to(equal(0))
        }
    }
}
