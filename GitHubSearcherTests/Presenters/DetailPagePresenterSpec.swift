//
//  DetailPagePresenterSpec.swift
//  GitHubSearcherTests
//
//  Created by Chang Wen-Lung on 12.04.20.
//  Copyright © 2020 Accelgor. All rights reserved.
//

import Quick
import Nimble
import Fakery

@testable import GitHubSearcher

final class DetailPagePresenterSpec: QuickSpec {
    override func spec() {
        var presenter: DetailPagePresentationLogic!
        var displayerMock: DetailPageDisplayerMock!

        beforeEach {
            presenter = DetailPagePresenter(inputData: DetailPage.InputData(userName: Faker().name.name(), repositoryName: Faker().name.name()))
            displayerMock = DetailPageDisplayerMock()
            presenter.viewController = displayerMock
        }

        it("Should reload normally") {
            presenter.reload(response: ForksModel.Response(page: Faker().number.randomInt(), items: []))
            expect(displayerMock.reloadCallsCount).to(equal(1))
            expect(displayerMock.reloadReceivedViewModel?.items.count).to(equal(0))
        }

        it("Should present next normally") {
            presenter.present(
                response: ForksModel.Response(page: Faker().number.randomInt(), items: []),
                page: Faker().number.randomInt())

            expect(displayerMock.nextCallsCount).to(equal(1))
            expect(displayerMock.nextReceivedViewModel?.items.count).to(equal(0))
        }
    }
}
